package fr.uvsq.Hayani.SDNS;

import java.util.Vector;

public class NomMachine {

	private String machine;
	private String domaine;
	private String local;
	
	
	public String getLocal() {
		return local;
	}
	public void setLocal(String local) {
		this.local = local;
	}
	public String getDomaine() {
		return domaine;
	}
	public void setDomaine(String domaine) {
		this.domaine = domaine;
	}
	public String getMachine() {
		return machine;
	}
	public void setMachine(String machine) {
		this.machine = machine;
	}

	
	
	public NomMachine(String local, String domaine, String machine) {
		super();
		this.local = local;
		this.domaine = domaine;
		this.machine = machine;
	}
	
	
	
	@Override
	public String toString() {
		return " [" + machine + "." + domaine+ "." + local + "]";
	}
	
	
	
	
	public NomMachine() {
		super();
	}
	
	
	public NomMachine(String Nommachine) {
		 String[] value =Nommachine.split("\\.");
		 this.machine=value[0].toString();
		 this.domaine=value[1].toString();
		 this.local=value[2].toString();
	}
	
	
}
