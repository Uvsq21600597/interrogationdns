package fr.uvsq.Hayani.SDNS;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

public class ChercherAvecDomaineTrierSelonNomMachine implements Commande {

private DnsItem di;
private Vector <DnsItem> l;
	
	
	public Vector<DnsItem> getL() {
	return l;
}

public void setL(Vector<DnsItem> l) {
	this.l = l;
}

	public ChercherAvecDomaineTrierSelonNomMachine(DnsItem di) {
		super();
		this.di = di;
	}

	public ChercherAvecDomaineTrierSelonNomMachine() {
		di =new DnsItem();
	}
	
	private void SortAvecNomMachine(Vector<DnsItem> l) {
		
		Collections.sort(l, new Comparator<DnsItem>(){
			@Override
				public int compare(DnsItem o1, DnsItem o2) {
					
			return o1.getNomMachine().getMachine().toString().compareTo(o2.getNomMachine().getMachine().toString());
				
				}});
	}
	
	
	
	@Override
	public void execute() {
		DNS dns=new DNS();
		l = new Vector<DnsItem>();
		String str= this.di.getNomMachine().getDomaine()+"."+this.di.getNomMachine().getLocal();
		l=dns.GetItems(str);
		SortAvecNomMachine(l);
		
		System.out.printf("la liste des machines du domaine triée selon le nom des machines : [ ");
		for (int i = 0; i < l.size(); i++) {
			System.out.printf(l.get(i).getNomMachine().getMachine() +" ");
			}
		System.out.printf("]");
		System.out.println();
	}

	
	
	
}
