package fr.uvsq.Hayani.SDNS;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

public class ChercherAvecDomaineTrierSelonAdresseIp implements Commande {

private DnsItem di;
private Vector<DnsItem> l;
	
	public Vector<DnsItem> getL() {
	return l;
}

public void setL(Vector<DnsItem> l) {
	this.l = l;
}

	public ChercherAvecDomaineTrierSelonAdresseIp(DnsItem di) {
		super();
		this.di = di;
		l = new Vector<DnsItem>();
	}

	public ChercherAvecDomaineTrierSelonAdresseIp() {
		di =new DnsItem();
	}
	
	
	
	@Override
	public void execute() {
		try {
		DNS dns=new DNS();
		l = new Vector<DnsItem>();
		String str= this.di.getNomMachine().getDomaine()+"."+this.di.getNomMachine().getLocal();
		l=dns.GetItems(str);
		SortAvecAdresseIP(l);
		if(!l.isEmpty()){
			System.out.printf("la liste des machines du domaine triée selon les adresses IP : [ ");
		for (int i = 0; i < l.size(); i++) {
			System.out.printf(l.get(i).getAdresseIp().toString()+" " );
		}
		
		
		System.out.printf("]");
		System.out.println();
		}
		
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	
	}

	
private void SortAvecAdresseIP(Vector<DnsItem> l) {
		
	Collections.sort(l, new Comparator<DnsItem>(){
			@Override
			public int compare(DnsItem o1, DnsItem o2) {
			
			Vector<Integer>  v1 = new Vector<Integer>() ;
			v1=o1.getAdresseIp().getAdresseAvecSesChiffres();
				
			Vector<Integer>  v2 = new Vector<Integer>() ;
			v2=o2.getAdresseIp().getAdresseAvecSesChiffres();
			
			if( v1.get(0)>v2.get(0)) return 1;
			else if (v1.get(0)<v2.get(0)) return -1;
			else {
				if (v1.get(1)>v2.get(1)) return 1;
				else if (v1.get(1)<v2.get(1)) return -1;
				else {
					if (v1.get(2)>v2.get(2)) return 1;
					else if (v1.get(2)<v2.get(2)) return -1;
					else {
						if (v1.get(3)>v2.get(3)) return 1;
						else if (v1.get(3)<v2.get(3)) return -1;
						else {
							return 0;
							}}}}}});
			}}
				
				

