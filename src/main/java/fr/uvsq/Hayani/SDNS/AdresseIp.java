package fr.uvsq.Hayani.SDNS;

import java.util.Vector;

public class AdresseIp {

	private String AdresseIp;
	
	public AdresseIp(String adresseIp) {
	
		AdresseIp = adresseIp;
	}

	public String getAdresseIp() {
		return AdresseIp;
	}
	
	public Vector<Integer>  getAdresseAvecSesChiffres(){
		
		 Vector<Integer>  v = new Vector<Integer>();
		 String[] value =AdresseIp.split("\\.");
		 v.add((Integer.parseInt(value[0].toString())));
		 v.add((Integer.parseInt(value[1].toString())));
		 v.add((Integer.parseInt(value[2].toString())));
		 v.add((Integer.parseInt(value[3].toString())));
		 return v;
	}
	
	


	public void setAdresseIp(String adresseIp) {
		this.AdresseIp = adresseIp;
	}

	@Override
	public String toString() {
		return "[" + AdresseIp + "]";
	}
	
	
	
}
