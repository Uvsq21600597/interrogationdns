package fr.uvsq.Hayani.SDNS;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Vector;



public class DnsTUI {


private DnsItem di;


private final Map<String , Commande> commands ;


	public DnsTUI() {
	this.commands = new HashMap<>() ;
	this.commands.put ("adr.es.se.ip" , new ChercherAvecAdresseIP(di) ) ;
	this.commands.put ("ls" , new ChercherAvecDomaineTrierSelonNomMachine(di) ) ;
	this.commands.put ("ls -a" , new ChercherAvecDomaineTrierSelonAdresseIp(di) ) ;
	this.commands.put ("nom.qualifié.machine" , new ChercherAvecNomMachine(di)) ;
	
	
}
	
	
	public boolean isAdresseIp(String c){
		if( c.matches("\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}")) return true;
		return false;
		
	}
	
	public boolean isNomMachine(String c){
		if( c.matches("\\w*\\.\\w*\\.\\w*")) return true;
		return false;
		
	}

	public boolean isDomaine(String c) {
	if( c.matches("ls\\p{javaWhitespace}\\w*\\.\\w*")) return true;
	return false;
	}

	public boolean isDomaine2(String c) {
	if( c.matches("ls\\p{javaWhitespace}\\-a\\p{javaWhitespace}\\w*\\.\\w*")) return true;
	return false;
	}

	
	
	
	public Commande NextCommande(String name ) {
		
		di =new DnsItem();
		if (isAdresseIp(name))  {
			AdresseIp ai= new AdresseIp(name);
			di.setAdresseIp(ai);
			this.commands.put (name , new ChercherAvecAdresseIP(di)) ;
			return this.commands.get(name);
		}
		else if (isNomMachine(name))  {
			NomMachine nomMachine= new NomMachine(name);
			di.setNomMachine(nomMachine);
			this.commands.put (name , new ChercherAvecNomMachine(di)) ;
			return this.commands.get(name);
		}
		else if (isDomaine(name))  {
			
			 String[] value=name.split(" ");
			 String[] NomMachine =value[1].split("\\.");
	         
	         NomMachine nm =new NomMachine();
	    	 nm.setDomaine(NomMachine[0].toString());
	    	 nm.setLocal(NomMachine[1].toString());
	    	 di.setNomMachine(nm);
	    	this.commands.put (name,new ChercherAvecDomaineTrierSelonNomMachine(di)) ;
			return this.commands.get(name);
		}
		else if (isDomaine2(name))  {
			
			 String[] value=name.split(" ");
			 String[] NomMachine =value[2].split("\\.");
	         
	         NomMachine nm =new NomMachine();
	    	 nm.setDomaine(NomMachine[0].toString());
	    	 nm.setLocal(NomMachine[1].toString());
	    	 di.setNomMachine(nm);
	    	 this.commands.put (name,new ChercherAvecDomaineTrierSelonAdresseIp(di)) ;
			return this.commands.get(name);
		}
	System.out.println(name+" n'est pas reconnu en tant que commande ");
	return null;
	}




	public void  Afficher(Commande c) {
	if ( c!= null) c.execute();
			
		}



}