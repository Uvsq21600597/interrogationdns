package fr.uvsq.Hayani.SDNS;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import java.util.Vector;

public class DNS {

	protected Properties properties;
	protected InputStream in ;
	private Vector<DnsItem> list;
	
	public static Properties loadProperties() throws IOException, FileNotFoundException{
	      Properties properties = new Properties();

	      FileInputStream input = new FileInputStream("SimulerDNS.properties");
	      InputStream in = DNS.class.getResourceAsStream("SimulerDNS.properties");
	       try{
	    	   properties.load(input);
	    	   return properties;

	      }
	       finally{

	         input.close();

	      }

	   }
	
	public void charger (){
		
		
		try{
	         Properties prop = DNS.loadProperties();
	         FileReader in = new FileReader( prop.getProperty("file_name"));
		     BufferedReader br = new BufferedReader(in);
		     String line;
		     while ((line = br.readLine()) != null) {
		        
		    	 String[] value=line.split(" ");
		         String[] NomMachine =value[0].split("\\.");
		         
		         NomMachine nm =new NomMachine();
		    	 nm.setMachine(NomMachine[0].toString());
		    	 nm.setDomaine(NomMachine[1].toString());
		    	 nm.setLocal(NomMachine[2].toString());
		    	 
		    	 AdresseIp ai= new AdresseIp(value[1].toString());
		    	 
		    	 DnsItem di= new DnsItem(ai, nm);
		    	 
		    	 list.add(di);
		     }
		     in.close();
			}
	      catch(Exception e){
	         e.printStackTrace();
	      }
	}

	public DNS() {
		list = new Vector<DnsItem>();
		charger();
	}
	

	
	public  DnsItem GetItem(AdresseIp ai){
		for (int i = 0; i < list.size(); i++) {
		if (list.get(i).getAdresseIp().toString().equals(ai.toString())) {
		//System.out.println(list.get(i).getAdresseIp() +" "+ ai.toString());
		return list.get(i);
	}
	}
		System.out.println("L'adresse Ip n'existe pas");
		
	return null;
		
	}
	
	
	public  DnsItem GetItem(NomMachine nm){
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getNomMachine().toString().equals(nm.toString())) {
			return list.get(i);
		}
		}
		System.out.println("Nom de Machine n'existe pas");
		return null;
		
	}
	public  Vector<DnsItem> GetItems(String nm){
		
		String str;
		Vector<DnsItem> l=new Vector<DnsItem>();
		for (int i = 0; i < list.size(); i++) {
		str= list.get(i).getNomMachine().getDomaine()+"."+list.get(i).getNomMachine().getLocal();
		if (str.equals(nm)) l.add(list.get(i));
			
		}
		return l;
		
	}
	
	
	
	
	
	
	
	
	
}
