package fr.uvsq.Hayani.SDNS;

public class DnsItem {
	private AdresseIp adresseIp;
	private NomMachine nomMachine;
	
	public AdresseIp getAdresseIp() {
		return adresseIp;
	}
	
	public DnsItem(AdresseIp adresseIp, NomMachine momMachine) {
		this.adresseIp = adresseIp;
		this.nomMachine = momMachine;
	}

	public DnsItem() {
		// TODO Auto-generated constructor stub
	}

	public void setAdresseIp(AdresseIp adresseIp) {
		this.adresseIp = adresseIp;
	}
	

	
	public NomMachine getNomMachine() {
		return nomMachine;
	}

	public void setNomMachine(NomMachine nomMachine) {
		this.nomMachine = nomMachine;
	}

	@Override
	public String toString() {
		return "DnsItem [" + adresseIp + " || " + nomMachine+ " ]";
	}
	
	

}
