package fr.uvsq.Hayani.SDNS;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.Vector;

import org.junit.Before;
import org.junit.Test;

public class DNSTest {

	private DNS dns;

	@Before
	public void setUp() {
		dns = new DNS();
		
	}
	
	@Test
	public void ChercherAvecAdresseIp(){
		DnsItem di = new DnsItem(new AdresseIp("20.20.20.20"), new NomMachine("local", "youssef", "PC1"));
		DnsItem result=dns.GetItem(new AdresseIp("20.20.20.20"));
		assertEquals(di.toString(), result.toString()); 
		
	}
	@Test
	public void ChercherAvecNomdeMachineTest(){
		
		DnsItem di = new DnsItem(new AdresseIp("20.20.20.20"), new NomMachine("local", "youssef", "PC1"));
		DnsItem result=dns.GetItem( new NomMachine("local", "youssef", "PC1"));
		assertEquals(di.toString(), result.toString());
	}
	
	@Test
	public  void ChercherAvecDomaineTest(){
	
		
		Vector<DnsItem> result= new Vector<DnsItem>();
				
		result=dns.GetItems( "youssef.local");
		
		Vector<DnsItem> excepted = new Vector<DnsItem>();
		
		AdresseIp ai2 = new AdresseIp("100.100.100.100");
		NomMachine nm2= new NomMachine("local","youssef", "PC4");
		DnsItem di2 = new DnsItem();
		di2.setAdresseIp(ai2);
		di2.setNomMachine(nm2);
		excepted.add(di2);
		
		
		AdresseIp ai4= new AdresseIp("20.20.20.20");
		NomMachine nm4= new NomMachine("local","youssef", "PC1");
		DnsItem di4 = new DnsItem();
		di4.setAdresseIp(ai4);
		di4.setNomMachine(nm4);
		excepted.add(di4);
		
		AdresseIp ai1 = new AdresseIp("40.40.40.40");
		NomMachine nm1= new NomMachine("local","youssef", "PC3");
		DnsItem di1 = new DnsItem();
		di1.setAdresseIp(ai1);
		di1.setNomMachine(nm1);
		excepted.add(di1);
		
		
		
		AdresseIp ai3= new AdresseIp("88.88.88.88");
		NomMachine nm3= new NomMachine("local","youssef", "PC6");
		DnsItem di3 = new DnsItem();
		di3.setAdresseIp(ai3);
		di3.setNomMachine(nm3);
		excepted.add(di3);
		
		assertEquals(excepted.toString(), result.toString());
		
	}
	
	@Test
	public void LoadPropertiesTest() throws FileNotFoundException, IOException{
		
		Properties prop = DNS.loadProperties();
		assertEquals("BD.txt", prop.getProperty("file_name"));
   }
	

}
