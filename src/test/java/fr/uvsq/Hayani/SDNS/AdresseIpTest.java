package fr.uvsq.Hayani.SDNS;

import static org.junit.Assert.*;

import java.util.Vector;

import org.junit.Test;

public class AdresseIpTest {

	@Test
	public void test() {
		AdresseIp ai = new AdresseIp("192.168.3.5");
		Vector<Integer> excepted= new Vector<Integer>();
		excepted.add(192);
		excepted.add(168);
		excepted.add(1);
		excepted.add(1);
		
		Vector<Integer> result=ai.getAdresseAvecSesChiffres();
		
		for (int i = 0; i < excepted.size(); i++){
			
			assertTrue((excepted.get(i).equals(result.get(i))));
	}
		
	}

}
