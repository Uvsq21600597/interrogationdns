package fr.uvsq.Hayani.SDNS;

import static org.junit.Assert.*;

import org.junit.Test;

public class ChercherAvecNomMachineTest {


	@Test
	public void ChercherAvecNomMachine() {
		NomMachine nm = new NomMachine("local","youssef", "PC1");
		AdresseIp ai = new AdresseIp("20.20.20.20");
		DnsItem di = new DnsItem();
		di.setNomMachine(nm);
		ChercherAvecNomMachine cip= new ChercherAvecNomMachine(di);
		cip.execute();
		assertEquals(di.getAdresseIp().toString(), ai.toString());
	}
	

}
