package fr.uvsq.Hayani.SDNS;

import static org.junit.Assert.*;

import java.util.Vector;

import org.junit.Test;

public class NomMachineTest {

	@Test
	public void testNomMachine() {
		NomMachine nm = new NomMachine("PC1.youssef.local");
		NomMachine excepted = new NomMachine("local","youssef","PC1" );
		assertEquals(excepted.toString(),nm.toString());
	}

}
