package fr.uvsq.Hayani.SDNS;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class DnsTUITest {

	private DnsTUI diTUI;

	
	@Test
	public void isAdresseIp(){
		diTUI = new DnsTUI();
		assertEquals(true, diTUI.isAdresseIp("192.34.33.33"));
	}
	
	@Test
	public void isNomMachine(){
		diTUI = new DnsTUI();
		assertEquals(true, diTUI.isNomMachine("PC3.youssef.local"));
	}
	
	
	@Test
	public void isDomaine() {
		diTUI = new DnsTUI();
		assertEquals(true, diTUI.isDomaine("ls youssef.local"));
	}
	
	@Test
	public void isDomaine2() {
		diTUI = new DnsTUI();
		assertEquals(true, diTUI.isDomaine2("ls -a youssef.local"));
	}

		
		
	@Test
	public void NextCommande() {
		
		diTUI = new DnsTUI();
		NomMachine nm= new NomMachine("local","youssef",null);
		DnsItem di = new DnsItem();
		di.setNomMachine(nm);
		assertEquals(new ChercherAvecDomaineTrierSelonAdresseIp(di).getClass(),diTUI.NextCommande("ls -a Nesrine.local").getClass());
	}




	
	
}
