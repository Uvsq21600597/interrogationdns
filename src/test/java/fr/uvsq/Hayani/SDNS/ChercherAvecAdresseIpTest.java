package fr.uvsq.Hayani.SDNS;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ChercherAvecAdresseIpTest {

	 
	
	@Test
	public void ChercherAvecAdresseIPtest() {
		NomMachine nm = new NomMachine("local","youssef", "PC1");
		AdresseIp ai = new AdresseIp("20.20.20.20");
		DnsItem di = new DnsItem();
		di.setAdresseIp(ai);
		ChercherAvecAdresseIP cip= new ChercherAvecAdresseIP(di);
		cip.execute();
		assertEquals(di.getNomMachine().toString(), nm.toString());
	}
	
	

}
